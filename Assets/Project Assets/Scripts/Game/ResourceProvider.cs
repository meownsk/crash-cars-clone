﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectPrefab {
	[PrefabPath("ScrapPrefab")]
	carScrap
}

public enum WeaponType {
	[PrefabPath("weapons/machinegun")]
	machinegun,
	[PrefabPath("weapons/flamethrower")]
	flamethrower,
	[PrefabPath("weapons/cannon")]
	cannon,
	[PrefabPath("weapons/rocketgun")]
	rocketgun,
}

public enum PickupPrefab {
	[PrefabPath("pickups/Medkit")]
	medkit,
	[PrefabPath("pickups/CoinPickup")]
	coin,
	[PrefabPath("pickups/machinegun")]
	machinegun,
	[PrefabPath("pickups/flamethrower")]
	flamethrower,
	[PrefabPath("pickups/cannon")]
	cannon,
	[PrefabPath("pickups/rocketgun")]
	rocketgun,

}

public enum CarType {
	[PrefabPath("Cars/CarPawnLight")]
	light,
	[PrefabPath("Cars/CarPawnMedium")]
	medium,
	[PrefabPath("Cars/CarPawnSemiheavy")]
	semiheavy,
	[PrefabPath("Cars/CarPawnHeavy")]
	heavy,
}

public enum UIPrefab {
	[PrefabPath("HUD/Cursor")]
	playerCursor,
	[PrefabPath("UI/LoseScreen")]
	loseScreen,
	[PrefabPath("UI/HUD")]
	hud,
}

public class ResourceProvider
{
	static ResourceProvider instance = new ResourceProvider();

	Dictionary<Enum,GameObject> prefabs = new Dictionary<Enum, GameObject>();

	private static GameObject getPrefab(Enum type) {
		var found = instance.prefabs.ContainsKey (type);
		if (found) {
			var prefabGO = instance.prefabs [type];
			return prefabGO;
		} else {
			var prefabName = type.path ();
			var prefabGO = Resources.Load<GameObject> (prefabName);
			instance.prefabs[type] = prefabGO;
			return prefabGO;
		}
	}

	public static GameObject getPrefab(CarType enemyType) {
		return getPrefab (enemyType as Enum);
	}

	public static GameObject getPrefab(WeaponType weaponType) {
		return getPrefab (weaponType as Enum);
	}

	public static GameObject getPrefab(PickupPrefab pickupType) {
		return getPrefab (pickupType as Enum);
	}
	
	public static GameObject getPrefab(UIPrefab uiType) {
		return getPrefab (uiType as Enum);
	}

	public static GameObject getPrefab(ObjectPrefab objType) {
		return getPrefab (objType as Enum);
	}
}

