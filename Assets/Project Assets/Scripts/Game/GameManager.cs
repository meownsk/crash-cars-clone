﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public Transform pawn;
	public int numberOfEnemiesOnTheField = 5;
	public CarSpawnPoint[] spawnPoints;

	[SerializeField] float spawnInterval = 15f;

	void Start () {
		var pawnController = pawn.GetComponent<CarPawn> ();
		if (pawnController != null) {
			var ai = pawn.GetComponent<CarAI> ();
			if (ai != null)
				Destroy (ai);
			var control = pawn.gameObject.AddComponent<UserControl> ();
			pawn.tag = "Player";

			pawnController.dyingEnded += playerDied;

			Instantiate (ResourceProvider.getPrefab (UIPrefab.hud),pawn);
		}

		StartCoroutine (spawnEnemies ());
	}

	void playerDied(GameObject go) {
		var loseScenario = new GameObject("LoseScenario");
		var loseComponent = loseScenario.AddComponent<LoseScenario> ();
		loseComponent.m_playerPawn = go.GetComponent<CarPawn> ();
		loseComponent.startFragment ();
	}

	IEnumerator spawnEnemies() {
		while (true) {
			yield return new WaitForSeconds (spawnInterval);
			var carsOnField = GameObject.FindObjectsOfType<CarPawn> ();
			var numberToSpawn = numberOfEnemiesOnTheField - carsOnField.Length + 1;
			for (int i = 0; i < numberToSpawn; i++) {
				// choose spawner
				int choosedSpawnerIndex = 0;
				choosedSpawnerIndex = Random.Range (0, spawnPoints.Length);
				var choosedSpawner = spawnPoints [choosedSpawnerIndex];
				// choose enemy type
				CarType choosedType = (CarType)Random.Range (0, 4);
				choosedSpawner.AddEnemySpawnToQueue (choosedType, newOne => {
				});

				yield return null;
			}
		}
	}
}
