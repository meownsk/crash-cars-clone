﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoseScenario : InfoFragment {
	public CarPawn m_playerPawn;
	
	public override void startFragment () {
		screenPrefab = ResourceProvider.getPrefab (UIPrefab.loseScreen);
		base.startFragment ();

		m_text.text = "You gained "+m_playerPawn.coinNumber ()+" coins.\n\nTry one more time.";
	}

	public override void endFragment () {
		base.endFragment ();

		restartLevel ();
	}
}
