﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedkitPickup : PickUp {

	public float healAmount = 30.0f;

	override protected bool applyEffect (CarPawn character) {
		return character.heal (healAmount);
	}
}
