﻿using System;
using UnityEngine;

public class WeaponPickup: PickUp {
	public WeaponType weaponType;

	override protected bool applyEffect (CarPawn character) {
		var weaponPrefab = ResourceProvider.getPrefab (weaponType);
		return character.pickupShootable (weaponPrefab);
	}
}