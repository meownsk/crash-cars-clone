﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup: PickUp {
	override protected bool applyEffect (CarPawn character) {
		character.pickupCoin ();
		return true;
	}
}
