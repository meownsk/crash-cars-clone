﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCollider: MonoBehaviour {

	float k_bumpForce = 2000;

	public void OnCollisionEnter (Collision collision) {
		var anotherCar = collision.gameObject.GetComponentInParent<CarPawn> ();
		if (anotherCar != null) {
//			print ("car bumped");
			var bumpDirection = transform.forward;
			var carRig = anotherCar.GetComponent (typeof(Rigidbody));
			var collisionRig = collision.rigidbody;
			var bumpForce = k_bumpForce * CarPawn.k_massMultiplier;
			collision.rigidbody.AddExplosionForce (bumpForce,collision.contacts[0].point,2);
		}
	}
}
