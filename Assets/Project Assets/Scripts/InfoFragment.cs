﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

abstract public class InfoFragment: MonoBehaviour {
	public GameObject screenPrefab;
	protected GameObject m_screen;
	protected Text m_text;
	protected Button m_button;

	protected PauseActivator pauseActivator;

	protected void Awake() {
		pauseActivator = gameObject.AddComponent <PauseActivator> ();
	}

	virtual public void startFragment () {
		m_screen = Instantiate (screenPrefab);
		var screenComponent = m_screen.GetComponent <InfoFragmentScreen> ();
		m_text = screenComponent.text;
		m_button = screenComponent.button;
		m_button.onClick.AddListener(() => endFragment ());

//		UIApplication.setupArrowCursor ();

//		pauseActivator.ActivatePause ();
	}

	virtual public void endFragment () {
		Destroy (m_screen);

//		pauseActivator.DeactivatePause ();

//		UIApplication.setupNoCursor ();
	}

	protected void restartLevel() {
//		loadingScreen.SetActive (true);
//		AsyncOperation async = Application.LoadLevelAsync(SceneManager.GetActiveScene().name);
//		yield return async;
//
//		currentScreen.SetActive (false);
//		loadingScreen.SetActive (false);
		LevelManager.restartCurrentLevel();
	}
}
