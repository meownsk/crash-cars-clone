﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class CarSpawnPoint: MonoBehaviour {
	private bool m_canSpawn = true;
    //[HideInInspector] public bool hasSpawnedEnemies = false;
	Collider m_collider;
	int m_enemiesInside = 0;

	void Start() {
		m_collider = GetComponent<Collider> ();
		m_collider.isTrigger = true;
	}

//	void OnTriggerEnter(Collider other) {
//		var otherScript = other.GetComponentInParent <CarPawn>();
//        if (otherScript != null) {
//			m_canSpawn = false;
//        }
//	}
//
//	void OnTriggerExit(Collider other) {
//		var otherScript = other.GetComponent<CarPawn>();
//		if (otherScript != null) {
//			m_canSpawn = true;
//		}
//	}

	void OnTriggerStay(Collider other) {
		var otherScript = other.GetComponent<CarPawn>();
		if (otherScript != null) {
			m_enemiesInside++;
//			print ("detected enemy inside");
		}
	}

    void FixedUpdate() {
		m_enemiesInside = 0;
//		print ("enemy counter nulled");
	}

	public bool canSpawn() {
		return m_canSpawn;
	}

	public void AddEnemySpawnToQueue(CarType type, Action<GameObject> callback) {
		StartCoroutine (SpawnEnemyWhenPossible (type, callback));
	}

	IEnumerator SpawnEnemyWhenPossible(CarType type, Action<GameObject> callback) {
		while (m_enemiesInside > 0)
			yield return null;
		var spawned = SpawnEnemy (type);
		m_enemiesInside++;
		callback (spawned);
	}

	public GameObject SpawnEnemy(CarType type) {
		GameObject enemyObj = ResourceProvider.getPrefab (type);
		var enemy = Instantiate(enemyObj, transform.position, Quaternion.identity) as GameObject;
//		var cameraRot = Camera.main.transform.GetComponentInParent<AutoCam>().transform.rotation.eulerAngles;
//		enemy.transform.rotation = Quaternion.Euler (new Vector3 (0,cameraRot.y, 0));
		return enemy;
	}
}
