﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelHUD : MonoBehaviour {
//	public Transform playerControl;
	public Text coinsLbl;
	public Image healthBar;
	public RectTransform minesIconsContainer;

	private CarPawn m_player;

	void Start() {
		m_player = GetComponentInParent <CarPawn> ();
		UpdateCoinsLabel (0);
		m_player.m_inventory.coinsNumberChanged += UpdateCoinsLabel;
//		UIApplication.setupGameCursor ();
	}

	void UpdateCoinsLabel(int newValue) {
		coinsLbl.text = newValue.ToString () + " coins";
	}
}
