﻿using System;
using UnityEngine;

public class Health: MonoBehaviour {
	public float maxHealth = 100;
	public float healthPoints;

	void Start() {
		healthPoints = maxHealth;
	}

	// returns true if health drained completely
	public bool hit (float damageValue) {
		healthPoints -= damageValue;

		return healthPoints <= 0;
	}

	public bool heal(float healValue) {
		if (healthPoints == maxHealth)
			return false;
		else {
			float newHPVal = healthPoints + healValue;
			healthPoints = newHPVal > maxHealth ? maxHealth : newHPVal;
			return true;
		}
	}
}

public delegate void HealthEvent(GameObject go);

public interface IHittable {
	void receiveBumpHit(float damageValue);
	void receiveWeaponHit(float damageValue);
	event HealthEvent dyingEnded;
}
