﻿using System;
using UnityEngine;

public enum FragmentState {
	inactive,
	started,
	finishing,
	ended
}

abstract public class ScenarioFragment: MonoBehaviour {
	public delegate void FragmentChange(GameObject e);
	public event FragmentChange FragmentStarted;
	public event FragmentChange TaskFailed;
	public event FragmentChange FragmentEnded;

	public FragmentState state = FragmentState.inactive;

	public string readableGoal;

	protected Transform widget;

	virtual public void startFragment() {
		state = FragmentState.started;

		if (FragmentStarted != null)
			FragmentStarted (gameObject);
	}

	virtual public void endFragment() {
		state = FragmentState.ended;
		
		if (FragmentEnded != null)
			FragmentEnded (gameObject);
	}

	virtual public void failTask() {
		if (TaskFailed != null)
			TaskFailed (gameObject);
	}
}
