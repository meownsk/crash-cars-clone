﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager {
	static LevelManager shared = new LevelManager();

	public static void startLevel(string levelName) {
		// load scene with loading screen
		Time.timeScale = 1;
		SceneManager.LoadSceneAsync ("Loading", LoadSceneMode.Single);
		// async load desired scene
		SceneManager.LoadSceneAsync (levelName);
	}

	public static void restartCurrentLevel() {
		var sceneName = SceneManager.GetActiveScene ().name;
		startLevel (sceneName);
	}

	public static void openMainMenu() {
		Time.timeScale = 1;
		SceneManager.LoadScene ("MainMenu");
	}
}

