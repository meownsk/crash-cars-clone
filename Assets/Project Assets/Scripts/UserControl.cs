﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

//[RequireComponent(typeof (CarPawn))]
public class UserControl : MonoBehaviour {

	public float simultaneousInputDelay = 0.5f;
	CarPawn pawnController; // the car controller we want to use

	void Start () {
		pawnController = GetComponent<CarPawn> ();
	}

	bool savedInput = false;
	float savedDirection = 0;
	IEnumerator saveCoroutine;
	float lastInput = 0;
	void Update () {
		// pass the input to the car!
		var left = CrossPlatformInputManager.GetAxis ("Turn left");
		var right = CrossPlatformInputManager.GetAxis ("Turn right");
		bool hasInput = left != 0 || right != 0;
		bool moveLeft = left > 0 && right == 0;
		bool moveRight = right > 0 && left == 0;
		bool moveBack = left != 0 && right != 0;

		float h = 0;
		if (moveLeft)
			h = -1;
		else if (moveRight)
			h = 1;
		if (moveBack && savedInput)
			h = savedDirection;
		
		if (hasInput) {
			// save input if not yet
			if (lastInput == 0 && hasInput) {
				savedDirection = h;
				saveCoroutine = SaveInput ();
				StartCoroutine (saveCoroutine);
			}
		} else {
			savedInput = false;
			savedDirection = 0;
			if (saveCoroutine != null) {
				StopCoroutine (saveCoroutine);
				saveCoroutine = null;
			}
		}

		pawnController.move(h, moveBack);
		lastInput = h;

		var shoot = CrossPlatformInputManager.GetAxis ("Fire");
		if (shoot > 0.5)
			pawnController.startShooting ();
		else
			pawnController.stopShooting ();
	}

	IEnumerator SaveInput() {
		yield return new WaitForSeconds (simultaneousInputDelay);
		savedInput = true;
		saveCoroutine = null;
	}
}
