﻿using UnityEngine;

public delegate void ShootableClipEvent(int clip);
public delegate void ShootableEvent(GameObject s);

public interface IShootable {
	event ShootableClipEvent clipFillingCahnged;
	event ShootableEvent clipEmpty;
	event ShootableEvent startedShooting;
	event ShootableEvent endedShooting;

	void startShooting ();
	void stopShooting ();
}
