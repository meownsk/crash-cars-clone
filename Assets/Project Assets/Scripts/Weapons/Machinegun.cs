﻿using System;
using UnityEngine;

public class Machinegun : AbstractWeapon {

	protected override void shoot () {
			base.shoot ();

			Vector3 bulletDirection = transform.forward; //new Vector3 (direction.x, 0, direction.y);
			float spawnOffset = 2f;
			Transform bulletTr = Instantiate (bulletPrefab, transform.position + bulletDirection * spawnOffset, transform.rotation).transform;
			Bullet bullet = bulletTr.GetComponent<Bullet> ();
			bullet.sender = gameObject;
			bullet.initialSpeed = bulletSpeed;
			bullet.initialDirection = bulletDirection;
			bullet.damage = bulletDamage;
	}
}
