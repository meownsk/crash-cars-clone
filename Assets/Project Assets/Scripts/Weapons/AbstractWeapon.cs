﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractWeapon : MonoBehaviour, IShootable {
	public event ShootableClipEvent clipFillingCahnged;
	public event ShootableEvent clipEmpty;
	public event ShootableEvent startedShooting;
	public event ShootableEvent endedShooting;
	
	public int clipVolume = 1;
	public float bulletSpeed = 10;
	public float bulletDamage = 1;
	public float shotsPerSecond;
	[SerializeField] protected GameObject bulletPrefab;

	private float m_shootingSpeed;
//	private AudioSource m_shotSound;
	IEnumerator m_shootingCoroutine;
	bool m_shooting = false;
	[SerializeField] int m_clipFill;

	// Use this for initialization
	void Start () {
//		m_shotSound = GetComponent<AudioSource>();
		m_clipFill = clipVolume;
		m_shootingSpeed = 1 / shotsPerSecond;
	}

	public bool isShooting() {
		return m_shootingCoroutine != null;
	}

	public void startShooting() {
		m_shooting = true;
		if (m_shootingCoroutine == null) {
			m_shootingCoroutine = StartShooting ();
			StartCoroutine (m_shootingCoroutine);
		}

		if (startedShooting != null)
			startedShooting (gameObject);
	}

	public void stopShooting () {
		m_shooting = false;
	}

	IEnumerator StartShooting () {
		while (m_shooting && m_clipFill > 0) {
			shoot ();

			if (clipFillingCahnged != null)
				clipFillingCahnged (m_clipFill);

			//			float shootRate = shootingSpeed;
			//			print (string.Format ("waiting for {0} seconds before next shoot", shootRate));
			yield return new WaitForSeconds (m_shootingSpeed);
		}
		m_shootingCoroutine = null;
		m_shooting = false; // in case when we used all clips, but still trying to shoot

		if (endedShooting != null)
			endedShooting (gameObject);

		if (m_clipFill == 0 && clipEmpty != null) {
			clipEmpty (gameObject);
		}
	}

	protected virtual void shoot() {
//		m_shotSound.Play ();
		--m_clipFill;
	}
}
