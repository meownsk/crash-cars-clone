﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAbove : CameraFacingBillboard {
	public Transform parentObject;

	Renderer parentRenderer;
	Transform container;

	void Start() {
		if (parentObject == null)
			parentObject = transform.parent;
		parentRenderer = parentObject.GetComponentInChildren<Renderer> ();

		base.Start ();
		container = transform.parent;
	}

	void Update () {
		base.Update ();

		var parentCenter = parentRenderer.bounds.center;
		var upperPoint = parentRenderer.bounds.ClosestPoint (parentCenter + new Vector3 (0, 5, 0));
		var textPos = upperPoint + new Vector3 (0, 1, 0);
		// set self position
		container.position = textPos;
	}
}
