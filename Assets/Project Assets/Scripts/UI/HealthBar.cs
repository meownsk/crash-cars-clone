﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	public GameObject observedObject;
	public bool autoInit;

	private Health m_observed;
	private Image m_image;

	void Start () {
		m_image = GetComponentInChildren<Image> ();
		m_observed = autoInit ? GetComponentInParent<Health>() : observedObject.GetComponent<Health>();
	}

	void Update () {
		m_image.fillAmount = m_observed.healthPoints / m_observed.maxHealth;
	}
}
