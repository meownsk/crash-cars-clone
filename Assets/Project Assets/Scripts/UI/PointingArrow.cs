﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointingArrow : MonoBehaviour {
	private Image m_image;

	void Start () {
		m_image = GetComponentInChildren<Image> ();
	}

	void Update () {
		var screenPos = Camera.main.WorldToScreenPoint(transform.parent.position);
		screenPos.y *= Mathf.Sign (screenPos.z);
		screenPos.x = Mathf.Sign (screenPos.z) > 0 ? screenPos.x : Screen.width - screenPos.x;
//		print (screenPos);
		if (screenPos.x > 0 && screenPos.x < Screen.width && screenPos.y > 0 && screenPos.y < Screen.height)
			m_image.gameObject.SetActive (false);
		else {
			m_image.gameObject.SetActive (true);

			var dirVector = new Vector3(screenPos.x - Screen.width/2, screenPos.y - Screen.height/2,0);
			float boundOffset = 20;
			var screenRect = new Rect (0, 0, Screen.width, Screen.height);
			Vector2 intersectPoint = new Vector2();
			screenRect.EllipseIntersection (screenRect.center, new Vector2(screenPos.x,screenPos.y), ref intersectPoint);
			var toBoundVec = intersectPoint - screenRect.center;
			toBoundVec *= (toBoundVec.magnitude - boundOffset) / toBoundVec.magnitude;
			m_image.rectTransform.anchoredPosition = screenRect.center + toBoundVec;
			m_image.rectTransform.rotation = Quaternion.FromToRotation(Vector3.up, dirVector);
		}
	}
}
