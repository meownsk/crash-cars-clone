﻿using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.Cameras;

public enum Direction {
	up,
	upRight,
	right,
	downRight,
	down,
	downLeft,
	left,
	upLeft,
	none
};

public static class Extensions {

	public static void makeVisible(this GameObject go, bool visible) {
		var sprites = go.GetComponentsInChildren<SpriteRenderer> ();
		foreach (var sprite in sprites) {
			sprite.enabled = visible;
		}
		var meshes = go.GetComponentsInChildren<MeshRenderer> ();
		foreach (var mesh in meshes) {
			mesh.enabled = visible;
		}
	}

	public static void ForceAudioPlay(this AudioSource audio) {
		audio.ignoreListenerPause = true;
		audio.Play ();
	}

	public static String path(this Enum enumValue) {
		var memberInfo = enumValue.GetType ().GetMember (enumValue.ToString ())[0];
		var attrType = typeof(PrefabPathAttribute);
		var attrVal = Attribute.GetCustomAttribute (memberInfo, attrType);
		if (attrVal != null) {
			var path = (attrVal as PrefabPathAttribute).path;
			return path;
		} else
			return "";
	}

	public static bool LineIntersection(this Rect rectangle, Vector2 lineStartPoint, Vector2 lineEndPoint, ref Vector2 result)
	{
		Vector2 minXLinePoint = lineStartPoint.x <= lineEndPoint.x ? lineStartPoint : lineEndPoint;
		Vector2 maxXLinePoint = lineStartPoint.x <= lineEndPoint.x ? lineEndPoint : lineStartPoint;
		Vector2 minYLinePoint = lineStartPoint.y <= lineEndPoint.y ? lineStartPoint : lineEndPoint;
		Vector2 maxYLinePoint = lineStartPoint.y <= lineEndPoint.y ? lineEndPoint : lineStartPoint;

		double rectMaxX = rectangle.xMax;
		double rectMinX = rectangle.xMin;
		double rectMaxY = rectangle.yMax;
		double rectMinY = rectangle.yMin;

		if (minXLinePoint.x <= rectMaxX && rectMaxX <= maxXLinePoint.x)
		{
			double m = (maxXLinePoint.y - minXLinePoint.y) / (maxXLinePoint.x - minXLinePoint.x);

			double intersectionY = ((rectMaxX - minXLinePoint.x) * m) + minXLinePoint.y;

			if (minYLinePoint.y <= intersectionY && intersectionY <= maxYLinePoint.y
				&& rectMinY <= intersectionY && intersectionY <= rectMaxY)
			{
				result = new Vector2((float)rectMaxX, (float)intersectionY);

				return true;
			}
		}

		if (minXLinePoint.x <= rectMinX && rectMinX <= maxXLinePoint.x)
		{
			double m = (maxXLinePoint.y - minXLinePoint.y) / (maxXLinePoint.x - minXLinePoint.x);

			double intersectionY = ((rectMinX - minXLinePoint.x) * m) + minXLinePoint.y;

			if (minYLinePoint.y <= intersectionY && intersectionY <= maxYLinePoint.y
				&& rectMinY <= intersectionY && intersectionY <= rectMaxY)
			{
				result = new Vector2((float)rectMinX, (float)intersectionY);

				return true;
			}
		}

		if (minYLinePoint.y <= rectMaxY && rectMaxY <= maxYLinePoint.y)
		{
			double rm = (maxYLinePoint.x - minYLinePoint.x) / (maxYLinePoint.y - minYLinePoint.y);

			double intersectionX = ((rectMaxY - minYLinePoint.y) * rm) + minYLinePoint.x;

			if (minXLinePoint.x <= intersectionX && intersectionX <= maxXLinePoint.x
				&& rectMinX <= intersectionX && intersectionX <= rectMaxX)
			{
				result = new Vector2((float)intersectionX, (float)rectMaxY);

				return true;
			}
		}

		if (minYLinePoint.y <= rectMinY && rectMinY <= maxYLinePoint.y)
		{
			double rm = (maxYLinePoint.x - minYLinePoint.x) / (maxYLinePoint.y - minYLinePoint.y);

			double intersectionX = ((rectMinY - minYLinePoint.y) * rm) + minYLinePoint.x;

			if (minXLinePoint.x <= intersectionX && intersectionX <= maxXLinePoint.x
				&& rectMinX <= intersectionX && intersectionX <= rectMaxX)
			{
				result = new Vector2((float)intersectionX, (float)rectMinY);

				return true;
			}
		}

		return false;
	}

	public static bool EllipseIntersection(this Rect rectangle, Vector2 lineStartPoint, Vector2 lineEndPoint, ref Vector2 result) {
		var vec = lineEndPoint - lineStartPoint;
		var tan = vec.y / vec.x;

		var A = rectangle.width / 2;
		var B = rectangle.height / 2;

		var x = A * B / (Mathf.Sqrt (B * B + A * A * Mathf.Pow (tan, 2)));
		var y = x * tan;
		var ellipsePoint = new Vector2 (x, y) * Mathf.Sign(vec.x);

		result = ellipsePoint + rectangle.center;
		return true;
	}
}
